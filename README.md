## Installation ##

Download project:

```bash
git clone https://gitlab.com/Creased/gitlab-runner /opt/gitlab-runner/
pushd /opt/gitlab-runner/
perl -p -i -e 's#\r##' $(git ls-files)

```

Generate configuration files (You'll need to edit `.env` file to match your setup !):

```bash
./install.sh

```

Set up the GitLab-Runner:

```bash
docker-compose up -d

```

Show status and log of service:

```bash
systemctl status hooked-on.service -l
journalctl -f -u hooked-on.service

```

#!/bin/bash

source .env

rm -f ${ROOT:-}/conf/config.toml

RUNNER_TOKEN=$(curl --silent -X POST -d "token=${API_TOKEN}" --url "${GITLAB_CI_URL}/api/v1/runners/register" | python3 -c "import sys, json; print(json.load(sys.stdin)['token'])")

cat <<-EOF >${ROOT:-}/conf/config.toml
concurrent = 1
check_interval = 0

[[runners]]
  name = "local-runner"
  url = "${GITLAB_CI_URL}"
  token = "${RUNNER_TOKEN}"
  executor = "docker"
  pre_clone_script = "git config --global http.proxy $HTTP_PROXY; git config --global https.proxy $HTTPS_PROXY"
  environment = ["HTTPS_PROXY=$HTTPS_PROXY", "HTTP_PROXY=$HTTP_PROXY", "https_proxy=$https_proxy", "http_proxy=$https_proxy", "NO_PROXY=$NO_PROXY", "no_proxy=$no_proxy"]
  [runners.docker]
    tls_verify = false
    image = "creased/gitlab-ci-base:latest"
    privileged = true
    disable_cache = false
    # volumes = ["/cache", "/etc/docker/certs.d:/etc/docker/certs.d"]
    volumes = ["/cache", "/var/run/docker.sock:/var/run/docker.sock"]
    environment = ["ENV=value", "LC_ALL=fr_FR.UTF-8"]
    disable_verbose = false
    dns = ["${DNS1}", "${DNS2}", "${DNS3}"]
  [runners.cache]

EOF

printf "\033[1;32m[+]\033[0m %s\n\n" "Configuration is done."

printf "%s\n\n\033[1;33m%s\033[0m\n\n%s\n\n\033[1;33m%s\033[0m\n" "Now you can start the GitLab-Runner using:" "docker-compose up -d" "and follow logs using:" "docker-compose logs --follow"

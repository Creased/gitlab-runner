update-ca-certificates --fresh
gitlab-ci-multi-runner restart
gitlab-ci-multi-runner status
gitlab-ci-multi-runner run --config /etc/gitlab-runner/config.toml --service gitlab-runner --user gitlab-runner
